package EventsWS.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import EventsWS.Model.EventsDAO;
import EventsWS.Model.beans.Event;
import EventsWS.Model.beans.User;

/**
 * Everything for the /event rest controller
 * 
 * @author KMcIvor
 *
 */
@RestController
@RequestMapping("/event")
public class EventController {

	
	@Autowired
	EventsDAO dao;

	/**
	 * mapping for /event : GET
	 * @return a list of all the events from the dao
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ArrayList<Event> getEvents() {

        return dao.getAllEvents();
	}
	
	/**
	 * Mapping for /event : POST
	 * 
	 * @param event the event to be added
	 * @return if the operation was successful
	 */
	@RequestMapping(method = RequestMethod.POST)
	public boolean addEvent(@RequestBody Event event){
		return dao.addEvent(event);
	}
	
	/**
	 * Mapping for /event : PUT
	 * @param event the event to update
	 * @return success
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public boolean editEvent(@RequestBody Event event){
		return dao.updateEvent(event);
	}
	
	/**
	 * Mapping for /event/{id} : GET
	 * 
	 * @param id the ID to display
	 * @return JSON object for the event
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Event getEvents(@PathVariable int id) {
		return dao.getEventByID(id);
	}

	/**
	 * Mapping for /event/{id} : DELETE
	 * @param id the ID to delete
	 * @return whether it was a success
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean deleteEvent(@PathVariable int id) {
		return dao.deleteEvent(id);
	}

	/**
	 * Mapping for /event/{id}/users : GET
	 * @param eventID the event ID to look at
	 * @return a list of the users attending this event
	 */
	@RequestMapping(value = "/{eventID}/users", method = RequestMethod.GET)
	public ArrayList<User> getUsersForEvent(@PathVariable int eventID){
		return dao.getEventByID(eventID).getUsersAttending();
	}
	
	/**
	 * Mapping for /event/{id}/users/{userID} : POST
	 * @param eventID the event ID to add a user to
	 * @param userID the user to add
	 * @return success
	 */
	@RequestMapping(value = "/{eventID}/users/{userID}", method = RequestMethod.POST)
	public boolean addUserToEvent(@PathVariable int eventID, @PathVariable int userID) {
		return dao.addUserToEvent(userID, eventID);
	}
	
	/**
	 * Mapping for /event/{id}/users/{userID} : DELETE
	 * @param eventID the event ID to remove the user from
	 * @param userID the user to add
	 * @return success
	 */
	@RequestMapping(value = "/{eventID}/users/{userID}", method = RequestMethod.DELETE)
	public boolean removeUserFromEvent(@PathVariable int eventID, @PathVariable int userID) {
		return dao.removeUserFromEvent(userID, eventID);
	}
	
	
	
	
	
}
