package EventsWS.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import EventsWS.Model.EventsDAO;
import EventsWS.Model.beans.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	EventsDAO dao;
	
	@RequestMapping(method = RequestMethod.GET)
	public ArrayList<User> getUsers() {
		return dao.getAllUsers();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public User getUsers(@PathVariable int id) {
		return dao.getUserByID(id);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean deleteUser(@PathVariable int id) {
		return dao.deleteUser(id);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addUser(@RequestBody User user){
		return dao.addUser(user);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public boolean editUser(@RequestBody User user){
		return dao.updateUser(user);
	}
}
