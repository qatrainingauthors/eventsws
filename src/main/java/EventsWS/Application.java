package EventsWS;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import EventsWS.Model.EventsDAO;
import EventsWS.Model.EventsDAOImpl;

@Configuration
@ComponentScan("EventsWS.Controller")
@EnableAutoConfiguration
public class Application {
	
	// start as a spring boot project
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public EventsDAO dao() {
		//return new EventsDAOHibernateImpl();
		return new EventsDAOImpl();
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		// properties for hibernate
		Properties prop = new Properties();
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		prop.setProperty("hibernate.dialect",
				"org.hibernate.dialect.MySQL5InnoDBDialect");
		prop.setProperty("hibernate.globally_quoted_identifiers", "true");
		prop.setProperty("hibernate.connection.autocommit", "false");
		prop.setProperty("show_sql", "true");

		// create a local session factory bean, scan the com.qa.beans
		// package for any annotated beans
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "EventsWS.Model" });
		sessionFactory.setHibernateProperties(prop);
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		// Use if you want an SQL Database
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost/events");
		dataSource.setUsername("root");
		//dataSource.setPassword("root");
		 dataSource.setPassword("");
		return dataSource;
	}
}
