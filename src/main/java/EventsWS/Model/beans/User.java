package EventsWS.Model.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userID;
	private String name;
	private String email;

//	@ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name="attending")
//	@JsonManagedReference
//	private List<Event> eventsAttending;

	public User(int userID, String name, String email) {
		super();
		this.userID = userID;
		this.name = name;
		this.email = email;
//		eventsAttending = new ArrayList<Event>();
	}

	public User() {
	}

//	public ArrayList<Event> getEventsAttending() {
//		return new ArrayList<Event>(eventsAttending);
//	}
//
//	public void addEvent(Event e) {
//		eventsAttending.add(e);
//	}
//
//	public void setEventsAttending(ArrayList<Event> list) {
//		this.eventsAttending = list;
//	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
