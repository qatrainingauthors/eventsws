package EventsWS.Model.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int eventID;
	private String name;
	private String description;
	private Date date;
	private int spaces;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="attending")
	private List<User> usersAttending;

	@JsonCreator
	public Event(@JsonProperty("eventID") int eventID,
			@JsonProperty("name") String name,
			@JsonProperty("description") String description,
			@JsonProperty("date") Date date, @JsonProperty("spaces") int spaces) {
		// public Event(int eventID, String name, String description, Date date,
		// int spaces) {
		this.eventID = eventID;
		this.name = name;
		this.description = description;
		this.date = date;
		this.spaces = spaces;
		this.usersAttending = new ArrayList<User>();

	}

	
	public Event() {
		usersAttending = new ArrayList<User>();
	}

	public int getEventID() {
		return eventID;
	}

	public void setEventID(int eventID) {
		this.eventID = eventID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getSpaces() {
		return spaces;
	}

	public void setSpaces(int spaces) {
		this.spaces = spaces;
	}

	public ArrayList<User> getUsersAttending() {
		return new ArrayList<User>(usersAttending);
	}

	public void setUsersAttending(ArrayList<User> usersAttending) {
		this.usersAttending = usersAttending;
	}

	public void addUser(User userID) {
		usersAttending.add(userID);
	}

}
