package EventsWS.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import EventsWS.Model.beans.Event;
import EventsWS.Model.beans.User;

public class EventsDAOImpl implements EventsDAO {

	ArrayList<Event> events;
	ArrayList<User> users;

	public EventsDAOImpl() {
		createDatabase();
	}
	
	public void createDatabase(){
		events = new ArrayList<Event>();
		users = new ArrayList<User>();

		events.add(new Event(0, "Birthday", "It's My Birthday",
				getDate("10-12-2014"), 5));
		events.add(new Event(1, "Party", "Horray for house parties",
				getDate("24-12-2014"), 15));
		events.add(new Event(2, "Cookie Time", "Chocolate Chip",
				getDate("1-12-2014"), 2));
		events.add(new Event(3, "Coding", "Coding for fun",
				getDate("1-12-2014"), 10));
		events.add(new Event(4, "Work", "Work work. Zug zug.",
				getDate("1-1-2015"), 24));

		users.add(new User(0, "Alice", "Alice@email.com"));
		users.add(new User(1, "Bob", "Bob@email.com"));
		users.add(new User(2, "Eve", "Eve@email.com"));
		users.add(new User(3, "Mallory", "Mallory@email.com"));

		events.get(0).addUser(users.get(0));
		events.get(0).addUser(users.get(1));
		events.get(0).addUser(users.get(2));
		events.get(0).addUser(users.get(3));
		events.get(2).addUser(users.get(1));
		events.get(2).addUser(users.get(3));
		events.get(3).addUser(users.get(1));
		events.get(3).addUser(users.get(0));
		events.get(4).addUser(users.get(1));
		events.get(4).addUser(users.get(2));
		events.get(4).addUser(users.get(3));
		events.get(4).addUser(users.get(0));
	}

	private Date getDate(String dateStr) {
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateStr);
		} catch (ParseException e) {
			return new Date();
		}
	}

	@Override
	public boolean addUserToEvent(int userID, int eventID) {
		// get the event
		// System.out.println("EventID : " + eventID);
		// System.out.println("UserID : " + userID);
		for (Event e : events) {
			// System.out.println("Current on event: " + e.getEventID());
			if (e.getEventID() == eventID) {

				// if there is space
				if ((e.getSpaces() - e.getUsersAttending().size()) > 0) {

					// get the user
					for (User u : users){
						if (u.getUserID() == userID){
							e.addUser(u);
							return true;
						}
					}
					// add the user, return true
//					e.addUser(userID);
//					return true;

					// System.out.println("Not found user");
				}
			}
		}
		System.out.println("Not found event");
		// else return false
		return false;
	}

	@Override
	public boolean removeUserFromEvent(int userID, int eventID) {
		for (Event e : events) {
			ArrayList<User> userList = e.getUsersAttending();
			if (e.getEventID() == eventID) {
				for (int x = 0; x < userList.size(); x++) {
					User u = userList.get(x);
					if (u.getUserID() == userID) {
						userList.remove(x);
						e.setUsersAttending(userList);
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public ArrayList<Event> getAllEvents() {
		return events;
	}

	@Override
	public ArrayList<User> getAllUsers() {
		return users;
	}

	@Override
	public boolean addEvent(Event e) {
		events.add(e);
		return true;
	}

	@Override
	public boolean updateEvent(Event e) {
		for (int x = 0; x < events.size(); x++) {
			Event event = events.get(x);
			if (event.getEventID() == e.getEventID()) {
				events.remove(x);
				events.add(e);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean deleteEvent(int eventID) {
		for (int x = 0; x < events.size(); x++) {
			Event event = events.get(x);
			if (event.getEventID() == eventID) {
				events.remove(x);
				return true;
			}
		}
		return false;
	}

	@Override
	public Event getEventByID(int ID) {
		for (Event e : events) {
			if (e.getEventID() == ID) {
				return e;
			}
		}
		return null;
	}

	@Override
	public User getUserByID(int ID) {
		for (User u : users) {
			if (u.getUserID() == ID) {
				return u;
			}
		}
		return null;
	}

	public boolean updateUser(User user) {
		for (int x = 0; x < users.size(); x++) {
			User u = users.get(x);
			if (u.getUserID() == user.getUserID()) {
				users.remove(x);
				users.add(user);
				return true;
			}
		}
		return false;
	}

	public boolean deleteUser(int u) {
//		System.out.println("deleting user");
		for (int x = 0; x < users.size(); x++) {
//			System.out.println("x: " + x);
			if (users.get(x).getUserID() == u) {
//				System.out.println("found it, deleting");
				users.remove(x);
//				System.out.println("returning");
				return true;
			}
		}
		System.out.println("didn't find it.");
		return false;
	}

	public boolean addUser(User u) {
		users.add(u);
		return true;
	}
}
