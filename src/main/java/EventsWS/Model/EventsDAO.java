package EventsWS.Model;

import java.util.ArrayList;

import EventsWS.Model.beans.Event;
import EventsWS.Model.beans.User;

public interface EventsDAO {
	//events
	/**
	 * This method should return a list of all the events in the DAO
	 * @return an array list of events
	 */
	public ArrayList<Event> getAllEvents();
	
	/**
	 * This method adds an event to the list of events in the dao
	 * @param e The new event to add
	 * @return whether the event was added successfully
	 */
	public boolean addEvent(Event e);
	
	/**
	 * updates an event based on the event's ID. It replaces the old
	 * event in its entirety
	 * 
	 * @param e The new event
	 * @return whether the method was successful, it may not be if the event ID was unable to be found
	 */
	public boolean updateEvent(Event e);
	
	/**
	 * Delete the event with the current ID
	 * @param eventID the ID of the event to delete
	 * @return whether the event was deleted. False means that the event was unable to be found
	 */
	public boolean deleteEvent(int eventID);
	
	/**
	 * Returns an event from the dao based on its ID. A return value of null means that the 
	 * event didn't exist in the dao.
	 * @param ID the ID of the event to return
	 * @return Either the event or a null value if it cannot be found
	 */
	public Event getEventByID(int ID);
	
	//users
	/**
	 * Get all the users in the DAO
	 * @return an array list of all the users
	 */
	public ArrayList<User> getAllUsers();
	
	/**
	 * Get a single user by its ID number, a null return value means that the user could not be found
	 * @param ID the ID of the user to return
	 * @return the User or a null value
	 */
	public User getUserByID(int ID);
	
	/**
	 * Update a user. The user is identified using the user ID. The old user is entirely
	 * replaced with the new user. 
	 * @param u the updated user to save
	 * @return whether the operation was successful. False means the user was unable to be found
	 */
	public boolean updateUser(User u);
	/**
	 * Delete the user with the correct UserID
	 * @param UserID ID of the user to delete
	 * @return whether it was successful or not. False means that the user could not be found
	 */
	public boolean deleteUser(int UserID);
	/**
	 * Add a user to the dao
	 * @param u the user to add
	 * @return whether the method was successful
	 */
	public boolean addUser(User u);
	
	//users interacting with events
	/**
	 * Gets the user corresponding with that userID and adds it to the list of
	 * users attending that event 
	 * @param userID the userID of the user
	 * @param eventID the event to add the user to
	 * @return whether the method was successful
	 */
	public boolean addUserToEvent(int userID, int eventID);
	
	/**
	 * Removes a user from the list of users attending an event
	 * @param userID the userID to remove from the event
	 * @param eventID the event to look up
	 * @return whether the method was successful
	 */
	public boolean removeUserFromEvent(int userID, int eventID);
	
	//create a database from scratch if one isn't there already
	/**
	 * Create the objects in the DAO or create a database with 
	 * some content
	 */
	public void createDatabase();
	
}
