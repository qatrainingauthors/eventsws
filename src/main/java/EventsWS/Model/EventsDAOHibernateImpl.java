package EventsWS.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import EventsWS.Model.beans.Event;
import EventsWS.Model.beans.User;

public class EventsDAOHibernateImpl implements EventsDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void createDatabase() {
		ArrayList<Event> events = new ArrayList<Event>();
		ArrayList<User> users = new ArrayList<User>();

		events.add(new Event(0, "Birthday", "It's My Birthday",
				getDate("10-12-2014"), 5));
		events.add(new Event(1, "Party", "Horray for house parties",
				getDate("24-12-2014"), 15));
		events.add(new Event(2, "Cookie Time", "Chocolate Chip",
				getDate("1-12-2014"), 2));
		events.add(new Event(3, "Coding", "Coding for fun",
				getDate("1-12-2014"), 10));
		events.add(new Event(4, "Work", "Work work. Zug zug.",
				getDate("1-1-2015"), 24));

		users.add(new User(0, "Alice", "Alice@email.com"));
		users.add(new User(1, "Bob", "Bob@email.com"));
		users.add(new User(2, "Eve", "Eve@email.com"));
		users.add(new User(3, "Mallory", "Mallory@email.com"));

		events.get(0).addUser(users.get(0));
		events.get(0).addUser(users.get(1));
		events.get(0).addUser(users.get(2));
		events.get(0).addUser(users.get(3));
		events.get(2).addUser(users.get(1));
		events.get(2).addUser(users.get(3));
		events.get(3).addUser(users.get(1));
		events.get(3).addUser(users.get(0));
		events.get(4).addUser(users.get(1));
		events.get(4).addUser(users.get(2));
		events.get(4).addUser(users.get(3));
		events.get(4).addUser(users.get(0));

		// as user has the link table definition then we need to ensure that
		// it is added this way around
		// users.get(0).addEvent(events.get(0));
		// users.get(0).addEvent(events.get(3));
		// users.get(0).addEvent(events.get(4));
		// users.get(1).addEvent(events.get(0));
		// users.get(1).addEvent(events.get(2));
		// users.get(1).addEvent(events.get(3));
		// users.get(1).addEvent(events.get(4));
		// users.get(2).addEvent(events.get(0));
		// users.get(2).addEvent(events.get(4));
		// users.get(3).addEvent(events.get(0));
		// users.get(3).addEvent(events.get(2));
		// users.get(3).addEvent(events.get(4));

		Session sess = sessionFactory.openSession();
		for (Event e : events) {
			System.out.println("Saving Event: " + e.getEventID());
			sess.save(e);
		}
		for (User u : users) {
			System.out.println("Saving User: " + u.getUserID());
			sess.save(u);
		}
		sess.flush();
		sess.close();
	}

	private Date getDate(String dateStr) {
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateStr);
		} catch (ParseException e) {
			return new Date();
		}
	}

	@Override
	public ArrayList<Event> getAllEvents() {
		System.out.println("returning all events");
		// another way of doing it.
		// sessionFactory.openSession().createCriteria(Event.class).list()
		Session sess = sessionFactory.openSession();
		Query qry = sess.createQuery("from Event");
		ArrayList<Event> list = new ArrayList<Event>(qry.list());
		sess.close();
		return list;
	}

	@Override
	public boolean addEvent(Event e) {
		sessionFactory.openSession().save(e);
		return true;
	}

	@Override
	public boolean updateEvent(Event e) {
		System.out.println(e.getDescription());
		Session sess = sessionFactory.openSession();
		sess.saveOrUpdate(e);
		sess.flush();
		sess.close();
		return true;
	}

	@Override
	public boolean deleteEvent(int eventID) {
		Query qry = sessionFactory.openSession().createQuery(
				"delete from Event where id = :eventID");
		qry.setInteger("eventID", eventID);
		int i = qry.executeUpdate();
		return (i > 0);
	}

	@Override
	public Event getEventByID(int ID) {
		Query qry = sessionFactory.openSession().createQuery(
				"from Event where eventID = :ID");
		qry.setInteger("ID", ID);
		Event event = (Event) qry.list().get(0);
		return event;
	}

	@Override
	public ArrayList<User> getAllUsers() {
		System.out.println("Returning all users");
		Query qry = sessionFactory.openSession().createQuery("from User");
		return new ArrayList<User>(qry.list());
	}

	@Override
	public User getUserByID(int ID) {
		Query qry = sessionFactory.openSession().createQuery(
				"from User where userID = :ID");
		qry.setInteger("ID", ID);
		User user = (User) qry.list().get(0);
		return user;
	}

	@Override
	public boolean updateUser(User u) {
		Session sess = sessionFactory.openSession();
		sess.saveOrUpdate(u);
		sess.flush();
		sess.close();
		return true;
	}

	@Override
	public boolean deleteUser(int userID) {
		Query qry = sessionFactory.openSession().createQuery(
				"delete from User where id = :userID");
		qry.setInteger("userID", userID);
		int i = qry.executeUpdate();
		return (i > 0);
	}

	@Override
	public boolean addUser(User u) {
		sessionFactory.openSession().save(u);
		return true;
	}

	@Override
	public boolean addUserToEvent(int userID, int eventID) {
		// we need to get the user object and add it to the event object before
		// saving
		// the event again
		Session sess = sessionFactory.openSession();

		// get the user
		Query qry = sess.createQuery("from User where userID = :ID");
		qry.setInteger("ID", userID);
		User u = (User) qry.list().get(0);

		// get the event
		Query qry2 = sess.createQuery("from Event where eventID = :ID");
		qry2.setInteger("ID", eventID);
		Event e = (Event) qry2.list().get(0);

		// add the user to the event
		e.addUser(u);

		// save it
		sess.saveOrUpdate(e);
		sess.flush();
		sess.close();

		return true;
	}

	@Override
	public boolean removeUserFromEvent(int userID, int eventID) {

		Session sess = sessionFactory.openSession();

		// get the event
		Query qry2 = sess.createQuery("from Event where eventID = :ID");
		qry2.setInteger("ID", eventID);
		Event e = (Event) qry2.list().get(0);
		System.out.println("got event attendees: " + e.getUsersAttending().size());
		
		ArrayList<User> userList = e.getUsersAttending();
		
		// add the user to the event
		for (int x = 0; x < userList.size(); x++) {
			System.out.println("x: " + x);
			User user = userList.get(x);
			System.out.println("User: " + user.getUserID());
			if (user.getUserID() == userID) {
				System.out.println("removing");
				userList.remove(x);
			}
		}
		
		e.setUsersAttending(userList);

		System.out.println("updating attendees: " + e.getUsersAttending().size());
		// save it
		sess.saveOrUpdate(e);
		sess.flush();
		sess.close();

		return true;
	}

}
