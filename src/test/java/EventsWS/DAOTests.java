package EventsWS;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import EventsWS.Model.EventsDAO;
import EventsWS.Model.EventsDAOImpl;
import EventsWS.Model.beans.Event;
import EventsWS.Model.beans.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DAOTests {

	EventsDAO dao;

	@Before
	public void setUpDAO() {
		this.dao = new EventsDAOImpl();
		dao.createDatabase();
	}

	@Test
	public void getAllEvents() {
		assertTrue("All Events Returned", dao.getAllEvents().size() == 5);
	}

	@Test
	public void addEvent() {
		// get size of list
		int sizeOfList = dao.getAllEvents().size();
		// add a new event
		dao.addEvent(new Event(9, "eventtest", "desctest", new Date(), 15));
		// size of list should have increased by one
		assertEquals(dao.getAllEvents().size(), sizeOfList + 1);
	}

	@Test
	public void updateEvent() {
		// add a new event
		dao.addEvent(new Event(9, "eventtest", "desctest", new Date(), 15));
		// create a new event which is different, but uses the same ID
		Event e = new Event(9, "Different!", "Different!", new Date(), 15);
		// update
		dao.updateEvent(e);
		// get event from the dao again
		Event e2 = dao.getEventByID(e.getEventID());

		// check change happened
		assertTrue(e2.getDescription().equalsIgnoreCase(e.getDescription()));
	}

	@Test
	public void deleteEvent() {
		// create an event
		Event e = new Event(9, "eventtest", "desctest", new Date(), 15);
		dao.addEvent(e);
		// check it is there
		Event e2 = dao.getEventByID(9);
		assertEquals(e, e2);

		// delete the event
		boolean worked = dao.deleteEvent(9);
		assertTrue(worked);

		// check the event is gone!
		Event e3 = dao.getEventByID(9);
		assertTrue(e3 == null);
	}

	@Test
	public void getEventByID() {
		// add an event
		Event e = new Event(9, "eventtest", "desctest", new Date(), 15);
		Event differentEvent = new Event(9, "lol", "desctest", new Date(), 15);
		dao.addEvent(e);
		// get the event out
		Event e2 = dao.getEventByID(9);
		// check they are the same event
		assertEquals(e, e2);
		assertNotEquals(e2, differentEvent);
	}

	@Test
	public void testGetAllUsers() {
		ArrayList<User> userList = dao.getAllUsers();
		Assert.assertTrue("user list is the right size", userList.size() == 4);
	}

	@Test
	public void getUserByID() {
		// add a new user
		User kat = new User(9, "kat", "kat@kat.com");
		User notKat = new User(10, "not kat", "blah");
		dao.addUser(kat);
		// get the user by ID
		User daoKat = dao.getUserByID(9);

		// check they are the same
		assertEquals(kat, daoKat);
		assertNotEquals(daoKat, notKat);
	}

	@Test
	public void updateUser() {
		User kat = new User(9, "kat", "kat@kat.com");
		User updatedKat = new User(9, "kat", "kat@differentemail.com");

		dao.addUser(kat);

		dao.updateUser(updatedKat);

		User daoKat = dao.getUserByID(9);

		assertEquals(daoKat.getEmail(), updatedKat.getEmail());
		assertNotEquals(daoKat.getEmail(), kat.getEmail());
	}

	@Test
	public void deleteUser() {
		// add a new user
		User kat = new User(9, "kat", "kat@kat.com");
		dao.addUser(kat);

		// get it from the dao
		// get the number of users from the DAO
		int users = dao.getAllUsers().size();
		// delete the user

		dao.deleteUser(9);
		// try to get it again
		assertTrue(dao.getAllUsers().size() == (users - 1));
	}

	public void addUser() {
		int before = dao.getAllUsers().size();

		User kat = new User(9, "kat", "kat@kat.com");
		dao.addUser(kat);

		assertTrue(dao.getAllUsers().size() == (before + 1));
	}

	// //users interacting with events

	@Test
	public void addUserToEvent() {
		// add a new event
		dao.addEvent(new Event(9, "test", "test", new Date(), 5));

		// add a new user
		dao.addUser(new User(10, "kat", "kat@kat.com"));

		// confirm the number of users for the new event is 0
		int before = dao.getEventByID(9).getUsersAttending().size();
		assertTrue(before == 0);

		// add the user to the event
		dao.addUserToEvent(10, 9);
		// confirm the number of users for the new event is 1
		int after = dao.getEventByID(9).getUsersAttending().size();

		assertTrue(after == 1);
	}

	// We should also have tests for when the event is full
	@Test
	public void testWhenEventIsFull() {
		// add an event with one space
		dao.addEvent(new Event(9, "test", "test", new Date(), 1));

		// add two users
		dao.addUser(new User(9, "kat", "kat@kat.com"));
		dao.addUser(new User(10, "notkat", "blah@blah.com"));

		// add one user to event, should work
		assertTrue(dao.addUserToEvent(9, 9));

		// add second user to event, should not work
		assertFalse(dao.addUserToEvent(10, 9));
	}

	@Test
	public void removeUserFromEvent() {
		dao.addEvent(new Event(9, "test", "test", new Date(), 1));
		dao.addUser(new User(9, "kat", "kat@kat.com"));
		
		dao.addUserToEvent(9, 9);
		
		int before = dao.getEventByID(9).getUsersAttending().size();
		
		dao.removeUserFromEvent(9, 9);
		
		assertTrue(dao.getEventByID(9).getUsersAttending().size() == (before -1));
	}

}
